package github

type Repository struct {
	Config GitHubConfig
}

type RepoConfig struct {
	Owner string
	Name  string
	Path  string
}

type BasicAuth struct {
	User     string
	Password string
}

type GitHubConfig struct {
	BasicAuth BasicAuth
	Repo      RepoConfig
}

type Commit struct {
	Files  []CommitFile `json:"files"`
	Commit struct {
		Author    Author `json:"author"`
		Committer Author `json:"committer"`
	} `json:"commit"`
	Author    Author `json:"author"`
	Committer Author `json:"committer"`
}

type CommitFile struct {
	Filename string `json:"filename"`
	RawUrl   string `json:"raw_url"`
}

type Author struct {
	ID        uint   `json:"id,omitempty"`
	Name      string `json:"name,omitempty"`
	Email     string `json:"email,omitempty"`
	DateTime  string `json:"date,omitempty"`
	Login     string `json:"login,omitempty"`
	AvatarUrl string `json:"avatar_url,omitempty"`
}

type Commiter struct {
	Author
}

type TreeContent struct {
	Sha   string     `json:"sha"`
	Items []TreeItem `json:"tree"`
}

type TreeItem struct {
	Path string `json:"path"`
	Type string `json:"type"`
}
