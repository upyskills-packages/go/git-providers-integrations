package github

import (
	b64 "encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

const BASE_URL = "https://api.github.com/repos"

func NewRepository(config GitHubConfig) *Repository {
	return &Repository{
		Config: config,
	}
}

func (r *Repository) GetFile(filePath string) (*RepoFile, error) {
	type apiFileResponse struct {
		Content string `json:"content"`
	}
	r.Config.Repo.SetPath(filePath)

	url := r.buildUrl()
	client := &http.Client{}
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("Authorization", "Basic "+basicAuth(r.Config.BasicAuth.User, r.Config.BasicAuth.Password))

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	} else if resp.StatusCode == 404 {
		return nil, errors.New(resp.Status)
	}
	defer resp.Body.Close()

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	data := &apiFileResponse{}
	if err := json.Unmarshal(content, &data); err != nil {
		return nil, nil
	}

	return &RepoFile{
		content:     data.Content,
		ContentType: resp.Header.Get("Content-Type"),
	}, nil
}

func (r *Repository) GetCommit(commitRef string) (*Commit, error) {
	url := r.buildUrlToCommitApi(commitRef)
	client := &http.Client{}
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("Authorization", "Basic "+basicAuth(r.Config.BasicAuth.User, r.Config.BasicAuth.Password))

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	} else if resp.StatusCode == 404 {
		return nil, errors.New(resp.Status)
	}
	defer resp.Body.Close()

	com := &Commit{}
	err = json.NewDecoder(resp.Body).Decode(com)
	return com, err
}

func (r *Repository) GetTreeContents(branchRef string) (*TreeContent, error) {
	url := r.buildUrlToContents(branchRef)
	fmt.Println(url)
	client := &http.Client{}
	req, _ := http.NewRequest("GET", url, nil)
	a := basicAuth(r.Config.BasicAuth.User, r.Config.BasicAuth.Password)
	fmt.Println(a)
	req.Header.Set("Authorization", "Basic "+a)

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	} else if resp.StatusCode == 404 {
		return nil, errors.New(resp.Status)
	}
	defer resp.Body.Close()

	content := TreeContent{}
	err = json.NewDecoder(resp.Body).Decode(&content)
	return &content, err
}

func (r *Repository) buildUrl() string {
	return fmt.Sprintf(
		"%s/%s/%s/contents/%s",
		BASE_URL,
		r.Config.Repo.Owner,
		r.Config.Repo.Name,
		r.Config.Repo.Path)
}

func (r *Repository) buildUrlToCommitApi(commitRef string) string {
	return fmt.Sprintf(
		"%s/%s/%s/commits/%s",
		BASE_URL,
		r.Config.Repo.Owner,
		r.Config.Repo.Name,
		commitRef)
}

func (r *Repository) buildUrlToContents(branchRef string) string {
	return fmt.Sprintf(
		"%s/%s/%s/git/trees/%s?recursive=1",
		BASE_URL,
		r.Config.Repo.Owner,
		r.Config.Repo.Name,
		branchRef)
}

func (c *RepoConfig) SetPath(path string) {
	c.Path = path
}

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return b64.StdEncoding.EncodeToString([]byte(auth))
}
