package github

import (
	b64 "encoding/base64"
)

type RepoFile struct {
	content     string
	ContentType string
}

func (f *RepoFile) ToString() string {
	bDec, err := b64.StdEncoding.DecodeString(f.content)
	if err != nil {
		return ""
	}
	return string(bDec)
}

func (f *RepoFile) ToBase64() string {
	return f.content
}

func (f *RepoFile) ToBytes() []byte {
	return []byte(f.ToString())
}
